class PostCard {
  constructor(post, user) {
    this.post = post;
    this.user = user;
  }

  createCard() {
    const card = document.createElement("div");
    card.className = "card";
    card.setAttribute("data-post-id", this.post.id);

    const headingCont = document.createElement("div");
    headingCont.className = "heading-cont";

    const title = document.createElement("h1");
    title.className = "card-heading";
    title.textContent = this.post.title;

    const deleteButton = document.createElement("img");
    deleteButton.className = "delete-button";
    deleteButton.src = "./delete_button.png";
    deleteButton.addEventListener("click", () => this.deleteTweet());

    const body = document.createElement("span");
    body.className = "post-text";
    body.textContent = this.post.body;

    const userContainer = document.createElement("div");
    userContainer.className = "user-container";

    const postBy = document.createElement("span");
    postBy.className = "post-by";
    postBy.textContent = "@" + this.user.name;

    const email = document.createElement("p");
    email.className = "email-by";
    email.textContent = " • email: " + this.user.email;

    userContainer.appendChild(postBy);
    userContainer.appendChild(email);
    headingCont.appendChild(title);
    headingCont.appendChild(deleteButton);

    card.appendChild(headingCont);
    card.appendChild(body);
    card.appendChild(userContainer);

    return card;
  }

  deleteTweet() {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    }).then((response) => {
      if (response.ok) {
        const tweetElement = document.querySelector(
          `[data-post-id="${this.post.id}"]`
        );
        tweetElement.remove();
      }
    });
  }
}

// Отримання даних користувачів та постів
fetch("https://ajax.test-danit.com/api/json/users")
  .then((response) => response.json())
  .then((users) => {
    fetch("https://ajax.test-danit.com/api/json/posts")
      .then((response) => response.json())
      .then((posts) => {
        const postContainer = document.querySelector(".posts");

        posts.forEach((post) => {
          const user = users.find((user) => user.id === post.userId);
          const postCard = new PostCard(post, user);
          const cardElement = postCard.createCard();
          postContainer.appendChild(cardElement);
        });
      });
  });
